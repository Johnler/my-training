# Destructuring
The  **destructuring assignment**  syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.
Example:

```js
	const person = {
		name: 'Jim',
		age: 21,
		address: 'Cebu City'
		gender: 'Male'
	}
	
	function display(obj) {
	const { name, age, address } = obj
	console.log('Name: ', name);
	console.log('Age: ', age);
	console.log('Address: ', adresss)
	}
	display(person); 
	//Output:
	/*
		Name: Jim
		Age: 21
		Address: Cebu City
	*/

```