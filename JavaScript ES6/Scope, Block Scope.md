# What is Scope?

-   In JavaScript there are two types of scope:
    
-   -   Local scope
-   -   Global scope
-   Scope determines the accessibility (visibility) of variables.
    
-   Each Functions creates new scope.
    
-   _Scope_ is the area of the _program_ where an item (be it variable, constant, function, etc.) that has an identifier name is recognized.
    
-   A **scope** in any **programming** is a region of the program where a defined variable can have its existence and beyond that variable it cannot be accessed.


## Scope

You could easily log this variable in the next line after the declaration.
```javascript
const message = 'Hello';
console.log(message); // 'Hello'
```

This time, when trying to log the variable, JavaScript throws  `ReferenceError: message is not defined`.
```javascript
if (true) {
  const message = 'Hello';
}
console.log(message); // ReferenceError: message is not defined
```
Why does it happen?

The  `if`  code block creates a  _scope_  for  `message`  variable. And  `message`  variable can be accessed  _only_  within this scope.



## Block scope
A code block in JavaScript defines a scope for variables declared using `let` and `const`:
```javascript
if (true) {
  // "if" block scope
  const message = 'Hello';
  console.log(message); // 'Hello'
}
console.log(message); // throws ReferenceError
```
The first  `console.log(message)`  correctly logs the variable because  `message`  is accessed from the scope where it is defined.

But the second  `console.log(message)`  throws a reference error because  `message`  variable is accessed outside of its scope: the variable doesn’t exist here.

The code block of  `if`,  `for`,  `while`  statements also create a scope.

### ### _var_  is not block scoped
The code block creates a scope for variables declared using `const` and `let`. However, that’s not the case of variables declared using `var`.
```javascript
if (true) {
  // "if" block scope
  var count = 0;
  console.log(count); // 0
}
console.log(count); // 0
```

A  _code block does not create a scope_  for  `var`  variables, but a  _function body does_. Read the previous sentence again, and try to remember it.

## Function scope
A function in JavaScript defines a scope for variables declared using `var`, `let` and `const`.
```javascript
function run() {
  // "run" function scope
  var message = 'Run, Forrest, Run!';
  console.log(message); // 'Run, Forrest, Run!'
}

run();
console.log(message); // throws ReferenceError
```
`run()`  function body creates a scope. The variable  `message`  is accessible inside of the function scope, but inaccessible outside.

Same way, a function body creates a scope for  `let`,  `const`  and even function declarations.

```javascript
function run() {
  // "run" function scope
  const two = 2;
  let count = 0;
  function run2() {}

  console.log(two);   // 2
  console.log(count); // 0
  console.log(run2);  // function
}

run();
console.log(two);   // throws ReferenceError
console.log(count); // throws ReferenceError
console.log(run2);  // throws ReferenceError
```

**Scopes can be nested**
```js
function run() {
// function scope
	const message = 'Brown Fox';
	if(true) {
	// "if" code block scope
		const friend = 'Sleeping Dog';
		message; // 'Brown Fox'
	}
friend; // ReferenceError
}
run();
```

## Global
The global scope is the outermost scope. It is accessible from any inner (aka _local_) scope.