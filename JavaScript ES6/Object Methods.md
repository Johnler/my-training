# Object
## The Object constructor creates an object wrapper for the given value.

const no = new Object({name: "jim", address: "Cebu"})

const {name, address} = no
console.log(name, address)
//expected output : jim Cebu

# Object Assign
## The Object.assign() method copies own properties from one or more source objects to a target object. It returns the modified target object.

const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// expected output: Object { a: 1, b: 4, c: 5 }

console.log(returnedTarget);
// expected output: Object { a: 1, b: 4, c: 5 }


# Object Entries
## The Object.entries() method returns an array of a given object's own enumerable string-keyed property [key, value] pairs. This is the same as iterating with a for...in loop, except that a for...in loop enumerates properties in the prototype chain as well).

    example # 1: 

        const object1 = {
            a: 'somestring',
            b: 42
        };

        for (const [key, value] of Object.entries(object1)) {
        console.log(`${key}: ${value}`);
        }

    example # 2: 

        // array like object
        const obj = { 0: 'a', 1: 'b', 2: 'c' };
        console.log(Object.entries(obj)); 
        //result [ ['0', 'a'], ['1', 'b'], ['2', 'c'] ]






