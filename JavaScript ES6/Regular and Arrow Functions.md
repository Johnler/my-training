# Functions
**Functions** are "self contained" modules of code that accomplish a specific task. **Functions** usually "take in" data, process it, and "return" a result. Once a **function** is written, it can be used over and over and over again. **Functions** can be "called" from the inside of other **functions**.

Regular functions created using function declarations or expressions are constructible and callable. Since regular functions are constructible, they can be called using the new keyword. However, the arrow functions are only callable and not constructible, i.e arrow functions can never be used as constructor functions.

**Functions** has to perform action or related action.
**Function** is block of organized and reusable code.

## Regular Function
```js
function name(param) {
	return param;
};
```
## Fat Arrow Function / Arrow Function
```js
const name = (param) => {
	return param
};
```



## Questions
### 1.What are the elements of Function ?