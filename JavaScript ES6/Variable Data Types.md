# Variable Data Types
<table border="2"><tbody><tr><td>Data Types</td>
				<td>Description</td>
				<td>Example</td>
			</tr><tr><td><code>String</code></td>
				<td>represents textual data</td>
				<td><code>'hello'</code>, <code>"hello world!"</code> etc</td>
			</tr><tr><td><code>Number</code></td>
				<td>an integer or a floating-point number</td>
				<td><code>3</code>, <code>3.234</code>, <code>3e-2</code> etc.</td>
			</tr><tr><td><code>BigInt</code></td>
				<td>an integer with arbitrary precision</td>
				<td><code>900719925124740999n</code> , <code>1n</code> etc.</td>
			</tr><tr><td><code>Boolean</code></td>
				<td>Any of two values: true or false</td>
				<td><code>true</code> and <code>false</code></td>
			</tr><tr><td><code>undefined</code></td>
				<td>a data type whose variable is not initialized</td>
				<td><code>let a;</code></td>
			</tr><tr><td><code>null</code></td>
				<td>denotes a <code>null</code> value</td>
				<td><code>let a = null;</code></td>
			</tr><tr><td><code>Object</code></td>
				<td>key-value pairs of collection of data</td>
				<td><code>let student = { };</code></td>
			</tr>
				</tr><tr><td><code>Array</code></td>
				<td>elements of data</td>
				<td><code>let student = [];</code></td>
			</tr>
</tbody></table>

## String
`String` is used to store text. In JavaScript, strings are surrounded by quotes:
-   Single quotes:  `'Hello'`
-   Double quotes:  `"Hello"`
-   Backticks:  `` `Hello` ``
```js
	let name = 'DNA';
	let address = "Gorrordo Ave.";
	const result = `Name: ${name} \nAddress: ${address}`;
```
Single quotes and double quotes are practically the same and you can use either of them.

Backticks are generally used when you need to include variables or expressions into a string. This is done by wrapping variables or expressions with  `${variable or expression}`  as shown above.

## Number
`Number` represents integer and floating numbers (decimals and exponentials).
```js
	let age = 20
	let temperature = 35.5
```
A number type can also be `+Infinity`, `-Infinity`, and `NaN` (not a number). For example,
```js
const number1 = 3/0;
console.log(number1); // Infinity

const number2 = -3/0;
console.log(number2); // -Infinity

// strings can't be divided by numbers
const number3 = "abc"/3; 
console.log(number3);  // NaN
```

## BigInt
In JavaScript,  `Number`  type can only represent numbers less than  **(253**  **- 1)**  and more than  **-(253**  **- 1)**. However, if you need to use a larger number than that, you can use the  `BigInt`  data type.

A  `BigInt`  number is created by appending  **n**  to the end of an integer.

```js
// BigInt value
const value1 = 900719925124740998n;

// Adding two big integers
const result1 = value1 + 1n;
console.log(result1); // "900719925124740999n"

const value2 = 900719925124740998n;

// Error! BitInt and number cannot be added
const result2 = value2 + 1; 
console.log(result2); 
```

## Boolean
This data type represents logical entities. `Boolean` represents one of two values: `true` or `false`. It is easier to think of it as a yes/no switch.
```js
const dataChecked = true;
const valueCounted = false;
```

## Undefined
The `undefined` data type represents **value that is not assigned**. If a variable is declared but the value is not assigned, then the value of that variable will be `undefined`.
```js
let name;
console.log(name); // undefined
```
It is also possible to explicitly assign a variable value `undefined`. 
```js
let name = undefined;
console.log(name); // undefined
```

## Null
In JavaScript, `null` is a special value that represents **empty** or **unknown value**.
```js
const number = null;
```
The code above suggests that the number variable is empty.

## Object
An  `object`  is a complex data type that allows us to store collections of data.

```js
const student = {
    firstName: 'Jake',
    lastName: null,
    class: 10
};
```

## Array
Data that contains a group of elements. These elements are all of the same or mixed data types.
(array is also a object type)
```js
const arr_name = ['John', 'Ler', 'Jim', 'Boy'] //array of string
const arr_num = [69, 420, 21.2] // array of numbers
const arr_mix = ['John', true, 21] // array of halo-halo
``` 


## typeof
To find the type of a variable, you can use the `typeof` operator
```js
const name = 'ram';
typeof(name); // returns "string"

const number = 4;
typeof(number); //returns "number"

const valueChecked = true;
typeof(valueChecked); //returns "boolean"

const a = null;
typeof(a); // returns "object"
```

# Primitive

In  [JavaScript](https://developer.mozilla.org/en-US/docs/Glossary/JavaScript), a  **primitive**  (primitive value, primitive data type) is data that is not an  [object](https://developer.mozilla.org/en-US/docs/Glossary/Object)  and has no  [methods](https://developer.mozilla.org/en-US/docs/Glossary/Method). There are 7 primitive data types:  [string](https://developer.mozilla.org/en-US/docs/Glossary/String),  [number](https://developer.mozilla.org/en-US/docs/Glossary/Number),  [bigint](https://developer.mozilla.org/en-US/docs/Glossary/BigInt),  [boolean](https://developer.mozilla.org/en-US/docs/Glossary/Boolean),  [undefined](https://developer.mozilla.org/en-US/docs/Glossary/undefined),  [symbol](https://developer.mozilla.org/en-US/docs/Glossary/Symbol), and [null](https://developer.mozilla.org/en-US/docs/Glossary/Null).

Most of the time, a primitive value is represented directly at the lowest level of the language implementation.

All primitives are  **immutable**, i.e., they cannot be altered. It is important not to confuse a primitive itself with a variable assigned a primitive value. The variable may be reassigned a new value, but the existing value can not be changed in the ways that objects, arrays, and functions can be altered.