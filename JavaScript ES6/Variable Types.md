# What is variable?
- In computer **programming**, a **variable** or scalar is a storage location (identified by a memory address) paired with an associated symbolic name, which contains some known or unknown quantity of information referred to as a value.
- **Variables** are used to store information to be referenced and manipulated in a computer **program**. They also provide a way of labeling data with a descriptive name, so our programs can be understood more clearly by the reader and ourselves. It is helpful to think of **variables** as containers that hold information.



## var
The **`var`  statement** declares a function-scoped or globally-scoped variable, optionally initializing it to a value.


## let
The  **`let`**  statement declares a block-scoped local variable, optionally initializing it to a value.


## const

**`const`** are block-scoped, much like variables declared using the **`[let]`** keyword. The value of a constant can't be changed through reassignment, and it can't be re-declared.

