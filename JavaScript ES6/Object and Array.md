# Objects and Array

## Objects
An object is a standalone entity, with properties and type.
Compare it with a person, for example. A person is an object, with properties. A person has a name, age, height, weight, etc. The same way, JavaScript objects can have properties, which define their characteristics.

```js
const person = {
	name: 'Ler',
	age: 19,
	height: 5.6,
	weight: 69,
}
```
***Follow up questions***
*1. How to display each properties in object?*
*2. How to mutate object?*
*3. How to compare object to object?*

## Array
Array is a single variable that is used to store different elements.
Unlike most languages where array is a reference to the multiple variable, in JavaScript array is a single variable that stores multiple elements.
```js
const fruits = ['apple', 'banana', 'ananas', 'strawberry']
```

***Follow up questions***
*1. How to mutate arrays?*
*2. What type is array?*