# Spread Operator
JavaScript, [spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax) refers to the use of an ellipsis of three dots (`…`) to expand an iterable object into the list of arguments.
The  `…`  spread operator is useful for many different routine tasks in JavaScript, including the following:

-   Copying an array
-   Concatenating or combining arrays
-   Using Math functions
-   Using an array as arguments
-   Adding an item to a list
-   Adding to state in React
-   Combining objects
-   Converting NodeList to an array

In each case, the spread syntax expands an iterable object, usually an array, though it can be used on any interable, including a string.

Example: 

```js
	//array
	const fruits = ['🍏', '🍎' '🍌', '🍉', '🍇', '🍓'];
	const veges = ['🥦', '🥬', '🥒'];
	const salad = [...fruits, ...veges];
	console.log("Salad: ", salad);
	// OUTPUT:
	// Salad: ["🍏", "🍎", "🍌", "🍉", "🍇", "🍓", "🥦", "🥬", "🥒"]


	//object
	const person_info1 = {
		name: 'John',
		gender: 'Male',
		age: 21
	}
	const person_info2 = {
		address: 'Pob Pardo',
		phone_number: 1234567890
	}
	const person_info = {...person_info1, ...person_info2 }
	console.log('Person Info:', person_info);
	//OUTPUT:
	// Person Info: Person Info: {name: "John", gender: "Male", age: 21, address: "Pob Pardo", phone_number: 1234567890}
```



# Rest Parameter
The **rest parameter** syntax allows a function to accept an indefinite number of arguments as an array.
Example:
```js
	function sumAll(...args) {
		let total = 0;
		for (let arg of args) total += arg;
		return total;
	}
	console.log(sumAll(1)); // 1
	console.log(sumAll(1, 2)); // 3
	console.log(sumAll(1, 2, 3)); // 6
```