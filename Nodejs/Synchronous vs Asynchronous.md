# Synchronous vs Asynchronous


## Synchronous
synchronous code is executed in sequence – each statement waits for the previous statement to finish before executing.

```js
    console.log('Hello')
    console.log('Elon')
    console.log('World')

    //OUTPUT: 
    // Hello
    // Elon
    // World
```

## Asynchronous
Asynchronous code doesn't have to wait – your program can continue to run. You do this to keep your site or app responsive, reducing waiting time for the user.

```js
    console.log('Hello')
    setTimeout(() => {
        console.log('Elon')
    }, 2000)
    console.log('World')

    //OUTPUT
    // Hello
    // World
    // Elon
```