# ExpressJS
Express is a minimal web application framework, and one of the most popular third-party node module.
Express.js, or simply Express, is a back end web application framework for Node.js.


*Hello World*
```typescript
    const express = require('express')
    const app = express()
    const PORT: number = 3000

    app.get('/', (request: any, response: any) => {
        return response.send('Hello World');
    })

app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`))
```