# Promise

A promise is an object that may produce a single value some time in the future : either a resolved value, or a reason that it's not resolved (e.g., a network error occurred). ... Promise users can attach callbacks to handle the fulfilled value or the reason for rejection.

Promise is an object. There are 3 states of the Promise object:


- **Pending**: Initial State, before the Promise succeeds or fails
- **Resolved**: Completed Promise
- **Rejected**: Failed Promise

![](../assets/images/promise.png)