# Callback
A callback is a function passed as an argument to another function. This technique allows a function to call another function. A callback function can run after another function has finished.

Callbacks are a great way to handle something after something else has been completed. ... If we want to execute a function right after the return of some other function, then callbacks can be used.


```typescript
type TCallback = (a:number, b:number) => number
	const sum = (a,b): number => {
		return a+b;
	}
	function main(a:number, b:number , callback:TCallback): number{
		return callback(a, b);
	}

	console.log(main(1, 1, sum)) //logs 2

```