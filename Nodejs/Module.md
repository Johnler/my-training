# Module
is nothing more than a Javascript file that exports its code.
Nodejs has its built-in modules.

## Node.js Module Types
Node.js includes three types of modules:

**Core Modules**
**Local Modules**
**Third Party Modules**


Node.js is a light weight framework. The core modules include bare minimum functionalities of Node.js. These core modules are compiled into its binary distribution and load automatically when Node.js process starts. However, you need to import the core module first in order to use it in your application.

<table>
    <tbody>
        <tr>
            <th>Core Module</th>
            <th>Description</th>
        <tr>
        <tr>
            <td>http</td>
            <td>http module includes classes, methods and events to create Node.js http server.</td>
        </tr>
        <tr>
            <td>url</td>
            <td>url module includes methods for URL resolution and parsing.</td>
        </tr>
        <tr>
            <td>querystring</td>
            <td>querystring module includes methods to deal with query string.</td>
        </tr>
        <tr>
            <td>path</td>
            <td>path module includes methods to deal with file paths.</td>
        </tr>
        <tr>
            <td>fs</td>
            <td>fs module includes classes, methods, and events to work with file I/O.</td>
        </tr>
        <tr>
            <td>util</td>
            <td>util module includes utility functions useful for programmers.</td>
        </tr>
    </tbody>
</table>



## How to use modules
The traditional way to import a module in node is to use the  ```require``` function.



```js
//COMMON JS
const http = require('http');
```

```js
//ES MODULES
import http from 'http';
```

