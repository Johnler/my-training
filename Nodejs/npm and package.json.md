# NPM
NPM stands for **Node Package Manager**. You can use to install remote packages to use in your code.

When you install Nodejs it also installed NPM.

*Example Installing Express Package:*
```bash
    npm install express
```


# package.json
Package.json contains metadata about your project.
Stores the information of the app.
Tracks of the dependencies that you use in your app.