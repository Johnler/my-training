# Props

```
Components receive data from outside with props.

Props are used to pass data.

Data from props is read-only.

Props can only be passed from parent component to child.
```

# State

```
State is for managing data.

State data can be modified by its own component, but is private (cannot be accessed from outside)

Modifying state should happen with the setState ( ) method
```
