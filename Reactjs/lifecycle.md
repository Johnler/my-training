# Overview of the lifecycle

![React Lifecycle](https://miro.medium.com/max/1400/1*u8hTumGAPQMYZIvfgQMfPA.jpeg)

## The lifecycle methods are hooks to allow you to read state changes and control UI updates. The lifecycle can be broken down into 3 categories:

```
1. Mounting: The component is being added to the DOM.
2. Updates: The component receives changes props or state and is called when the component is being re-rendered.
3. Unmounting: The component is being removed from the DOM.
```

# Mounting

```
1. constructor()
2. componentWillMount()
3. render()
4. componentDidMount()
```

# Updating

```
1.) componentWillReceiveProps()
2.) shouldComponentUpdate()
3.) componentWillUpdate()
4.) render()
5.) componentDidUpdate()
```

# Unmounting

```
1.) componentWillUnmount()
```

# constructor

```
 This method is called when your component is being created and before mounting (being added to the DOM). Its primary use is to initialize state and .bind(this) for the component’s methods. If you do neither of these, then there is no need for a constructor.
```

> Sample Code
> `class Clicker extends React.Component { constructor(props) { super(props); this.handleClick = this.handleClick.bind(this); this.state = { clicks: 0 }; } handleClick() { this.setState({ clicks: this.state.clicks + 1 }); } //... } `

# componentWillMount

```
 This method is executed right before a component is added to the DOM / render(). It is generally recommended that you use the constructor, but this method is still included in the API mostly for backwards compatibility.

 You should avoid calling any functions that cause side effects in this method as setState won’t trigger a change and there is no DOM to interact with.

 Note that this is also the only lifecycle method called on the server.
```

# componentDidMount

```
Your component has now been rendered and exists in the DOM. This is the point when you should initiate AJAX requests, add event listeners, and perform any set up that requires a DOM. Calling setState during this method or any time after will cause a re-render.
```

> Sample Code
> `componetDidMount() { fetch('https://gitconnected.com') .then((res) => { this.setState({ user: res.user }); }); }`

# componentWillReceiveProps

```
When your component receives new props from its parent, componentWillReceiveProps(nextProps) is triggered. This is a great time to check if there are changes in the incoming props when compared to your current props and trigger a state change based on the new values. A common use-case for this is resetting state based on a change.
```

> Sample Code
> ` componentWillReceiveProps(nextProps) { if (this.props.id !== nextProps.id) { this.setState({ feedContent: [] }); } }`

# shouldComponentUpdate

```
 This method exists purely for performance improvements. Renders and reconciliations are expensive in React. shouldComponentUpdate(nextProps, nextState) provides the developer the ability to return a boolean true/false from this method which controls whether React should perform the reconciliation operations and DOM updates.

 The default behavior is for React to render every update, which works in most cases. If shouldComponentUpdate() returns false, then componentWillUpdate(), render(), and componentDidUpdate() will not be invoked.
```

> Sample Code
> ` shouldComponentUpdate(nextProps, nextState) { return this.props.clicks !== nextProps.clicks; }`

# componentWillUpdate

```
React invokes this method immediately before rendering when new props or state are being received. There is not much use for componentWillUpdate(nextProps, nextState) and should probably be avoided (similar to componentWillMount). You should not do anything that would change the state at this point — use componentWillReceiveProps if you need to do anything before a render.

Note that this method is not called on the initial render.
```

# componentDidUpdate

```
Immediately after React builds you a shiny new UI, componentDidUpdate(prevProps, prevState) is invoked. This is a great time to interact with the DOM or instantiate a new network request based on what the new interface should look like.
```

# componentWillUnmount

```
Your component had a great life and now it’s time for it to leave the UI. This is the moment to clean up everything that was associated with adding and maintaining your component while it was living on the UI.
```
