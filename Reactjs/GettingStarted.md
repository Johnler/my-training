# Create React App

## Create React App is a comfortable environment for learning React, and is the best way to start building a new single-page application in React.

## It sets up your development environment so that you can use the latest JavaScript features, provides a nice developer experience, and optimizes your app for production. 


# JS Template
```
npx create-react-app app-name
cd my-app
npm start

```

# TS Template
```
npx create-react-app app-name --template typescript
cd my-app
npm start

```