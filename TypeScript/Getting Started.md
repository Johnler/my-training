[//]: <> (Compiled by: Johnler W. Gamboa)
[//]: <> (Compiled by: Mark Abeto)

# Variable Types
	- string
	- number
	- boolean
	- null
	- undefined
Example: 
```ts
	let fname:string = "Mark";
	let age:number = 100;
	let isVirgin:boolean = true;
```
	
# Arrays
Example:
```ts
	let batch4: string[] = ["Jimboy", "Grace", "Sean", "Dan", "Jerome"]
	// or
	let batch3: Array<string> = ["Androel", "Danillo", "Gladys", "J Ryan", "Jinky", "Lovely", "Paul"]
```

# Type alias & Interface

```ts
	//type alias
	type Status = "active" | "inactive";
	//interface
	interface IEmployee {
		emp_id: string;
		emp_name: string;
		emp_age: number;
		emp_status: Status;
	}
```

# Generic

```ts
class  Stack<T> {
	items: T[];
	constructor() {
		this.items = [];
	}

	push(item: T): number {
		return  this.items.push(item);
	}

	pop(): T | undefined {
		return  this.items.pop();
	}
	getStack(): void {
		console.log(this.items);
	}
}

// look how I instantiate the class Stack with <Generic> type string
const  stack_string = new  Stack<string>();
const stack_number = new Stack<number>();
//string
stack_string.push('Zee');
stack_string.getStack();
//number
stack_number.push(1);
stack_number.getStack();
	
```