# Xstate



# Finite State Machines
```
 A finite state machine is a mathematical model of computation that describes the behavior of a system that can be in only one state at any given time. 
```
# Steps

## 1.) Suppose we want to model a Light as a state machine. First, install XState using NPM or YARN
```
npm install xstate@latest --save
# or:
yarn add xstate@latest --save
```

## 2.) Then, in your project, import createMachine, which is a factory function that creates a state machine or statechart:
```
import { createMachine } from 'xstate'; 

const lightMachine = createMachine(/* ... */);
```
## 3.) We'll pass the machine configuration inside of createMachine(...). Since this is a hierarchical machine, we need to provide the:
## id - to identify the machine and set the base string for its child state node IDs
## initial - to specify the initial state node this machine should be in
## states - to define each of the child states:
```
import { createMachine, interpret } from 'xstate';

const lightMachine = createMachine({
  id: 'light',
  initial: 'idle',
  states: {
    idle: {},
    on: {},
    off: {}
  }
});

const lightService = interpret(lightMachine);
```

