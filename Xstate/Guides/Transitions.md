# Transitions

## A state transition defines what the next state is, given the current state and event. State transitions are defined on state nodes, in the on property:

```
const lightMachine = createMachine({
  id: 'light',
  initial: 'idle',
  states: {
    idle: {
        on:{
            CLICK: "light_on"
        }
    },
    light_on: {
        on:{
            CLICK: {
                target: "light_off"
            }
        }
    },
    light_off: {
        on:{
            CLICK: "light_on"
        }
    }
  }
});

```