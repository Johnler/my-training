# Invoking Services


## Expressing the entire app's behavior in a single machine can quickly become complex and unwieldy. It is natural (and encouraged!) to use multiple machines that communicate with each other to express complex logic instead. This closely resembles the Actor model (opens new window), where each machine instance is considered an "actor" that can send and receive events (messages) to and from other "actors" (such as Promises or other machines) and react to them.

```
const lightMachine = createMachine({
        id: 'light',
        initial: 'idle',
        context: {
            count: 0
        },
        invoke: {
            src: 'timer',
        },
        states: {
            idle: {
                // The 'beeping' activity will take place as long as the machine is in the 'idle' state
                activities: ['beeping'],
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target: "light_on"
                    },
                    TICK:{
                        actions: ['logTick'],
                    }
                }
            },
            light_on: {
                on:{
                    CLICK: {
                         actions: ['logLightOff'],
                        target: "light_off"
                    },
                    TICK:{
                        actions: ['logTick'],
                    }
                }
            },
            light_off: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target :"light_on"
                    },
                    TICK:{
                        actions: ['logTick'],
                    }
                }
            }
        }
    },
    {
        actions: {
            logLightOn: () => {
                console.log("logLightOn")
            },
            logLightOff: () => {
                console.log("logLightOff")
            },
            logTick: () => {
                console.log("Tick")
            }
        },
        activities: {
            beeping: () => {
                // Start the beeping activity
                const interval = setInterval(() => console.log('BEEP!'), 1000);

                // Return a function that stops the beeping activity
                return () => clearInterval(interval);
            }
        },
        services: {
            timer: (context: IContext, _) => (send: any) => {
                setInterval(() => send('TICK'), 1000);  
            }
        }
    }
);

```