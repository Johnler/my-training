# While finite states are well-defined in finite state machines and statecharts, state that represents quantitative data (e.g., arbitrary strings, numbers, objects, etc.) that can be potentially infinite is represented as extended state instead. This makes statecharts much more useful for real-life applications.

# In XState, extended state is known as context. Below is an example of how context is used to simulate filling a glass of water:

```
const lightMachine = createMachine({
        id: 'light',
        initial: 'idle',
        context: {
            count: 0
        },
        invoke: {
            src: 'timer',
        },
        states: {
            idle: {
                // The 'beeping' activity will take place as long as the machine is in the 'idle' state
                activities: ['beeping'],
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target: "light_on"
                    },
                    TICK:{
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            },
            light_on: {
                on:{
                    CLICK: {
                         actions: ['logLightOff'],
                        target: "light_off"
                    },
                    TICK:{
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            },
            light_off: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target :"light_on"
                    },
                    TICK:{
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            }
        }
    },
    {
        actions: {
            logLightOn: () => {
                console.log("logLightOn")
            },
            logLightOff: () => {
                console.log("logLightOff")
            },
            logTick: () => {
                console.log("Tick")
            },
            increeementCountToContext: assign({
                count: (context: IContext, _) =>  (context.count + 1)
            }), 
        },
        activities: {
            beeping: () => {
                // Start the beeping activity
                const interval = setInterval(() => console.log('BEEP!'), 1000);

                // Return a function that stops the beeping activity
                return () => clearInterval(interval);
            }
        },
        services: {
            timer: (context: IContext, _) => (send: any) => {
                setInterval(() => send('TICK'), 1000);  
            }
        }
    }
);

```