# Guarded Transitions
# Many times, you'll want a transition between states to only take place if certain conditions on the state (finite or extended) or the event are met.

# This is a good use case for a "guarded transition", which is a transition that only occurs if some condition (cond) passes. A transition with condition(s) is called a guarded transition.

```
const lightMachine = createMachine({
        id: 'light',
        initial: 'idle',
        context: {
            count: 0
        },
        invoke: {
            src: 'timer',
        },
        states: {
            idle: {
                // The 'beeping' activity will take place as long as the machine is in the 'idle' state
                activities: ['beeping'],
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target: "light_on"
                    },
                    TICK:{
                        cond: 'checkTimer',
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            },
            light_on: {
                on:{
                    CLICK: {
                         actions: ['logLightOff'],
                        target: "light_off"
                    },
                    TICK:{
                        cond: 'checkTimer',
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            },
            light_off: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target :"light_on"
                    },
                    TICK:{
                        cond: 'checkTimer',
                        actions: ['logTick', 'increeementCountToContext'],
                    }
                }
            }
        }
    },
    {
        actions: {
            logLightOn: () => {
                console.log("logLightOn")
            },
            logLightOff: () => {
                console.log("logLightOff")
            },
            logTick: () => {
                console.log("Tick")
            },
            increeementCountToContext: assign({
                count: (context: IContext, _) =>  (context.count + 1)
            }), 
        },
        activities: {
            beeping: () => {
                // Start the beeping activity
                const interval = setInterval(() => console.log('BEEP!'), 1000);

                // Return a function that stops the beeping activity
                return () => clearInterval(interval);
            }
        },
        services: {
            timer: (context: IContext, _) => (send: any) => {
                setInterval(() => send('TICK'), 1000);  
            }
        }
        guards:{
            checkTimer: (context, event) => {
                return (context.count == 3)
            }
        }
    }
);

```