# Activities

## An activity is an action that occurs over time, and can be started and stopped. According to Harel's original statecharts paper:


```
const lightMachine = createMachine({
        id: 'light',
        initial: 'idle',
        context: {
            count: 0
        }
        states: {
            idle: {
                // The 'beeping' activity will take place as long as the machine is in the 'idle' state
                activities: ['beeping'],
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target: "light_on"
                    }
                }
            },
            light_on: {
                on:{
                    CLICK: {
                         actions: ['logLightOff'],
                        target: "light_off"
                    }
                }
            },
            light_off: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target :"light_on"
                    }
                }
            }
        }
    },
    {
        actions: {
            logLightOn: () => {
                console.log("logLightOn")
            },
            logLightOff: () => {
                console.log("logLightOff")
            },
        },
        activities: {
            beeping: () => {
                // Start the beeping activity
                const interval = setInterval(() => console.log('BEEP!'), 1000);

                // Return a function that stops the beeping activity
                return () => clearInterval(interval);
            }
        }
    }
);

```