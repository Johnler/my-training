# Actions
## Actions are fire-and-forget effects. For a machine to be useful in a real-world application, effects need to occur to make things happen in the real world, such as rendering to a screen.

## Actions are not immediately triggered. Instead, the State object returned from machine.transition(...) will declaratively provide an array of .actions that an interpreter can then execute.


```
const lightMachine = createMachine({
        id: 'light',
        initial: 'idle',
        context: {
            count: 0
        }
        states: {
            idle: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target: "light_on"
                    }
                }
            },
            light_on: {
                on:{
                    CLICK: {
                         actions: ['logLightOff'],
                        target: "light_off"
                    }
                }
            },
            light_off: {
                on:{
                    CLICK: {
                        actions: ['logLightOn'],
                        target :"light_on"
                    }
                }
            }
        }
    },
    {
        actions: {
            logLightOn: () => {
                console.log("logLightOn")
            },
            logLightOff: () => {
                console.log("logLightOff")
            },
        }
    }
);

```