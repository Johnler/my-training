# An event is what causes a state machine to transition from its current state to its next state. All state transitions in a state machine are due to these events; state cannot change unless some stimulus (the event) causes it to change.

# An event is an object with a type property, signifying what type of event it is:

```
const lightMachine = createMachine({
  id: 'light',
  initial: 'idle',
  states: {
    idle: {
        on:{
            CLICK: {
                
            }
        }
    },
    light_on: {
        on:{
            CLICK: {
                
            }
        }
    },
    light_off: {
        on:{
            CLICK: {
                
            }
        }
    }
  }
});

```