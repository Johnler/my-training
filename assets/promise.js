const func1 = (data) => {
    return new Promise((resolve, reject) => {
  
      switch(data){
          case "WOi": resolve("Resolve"); break;
          default: reject("Reject"); break;
      }
    });
  };
  // console.log(func1())
   func1("data").then((data) => data).catch((data) => data);
  
  
  
  // fetch("IpAddress:Endpoint")
  
  // func1("WOi").then((data) => console.log(data)).catch((data) => console.log(data));
  
  
  