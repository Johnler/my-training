# DNA-Training

## Table of Contents
- **JavaScript ES6**
	- Variable Types
	- Variable Data Types
	- Regular and Arrow Functions
	- Scope, Block Scope
	- Default Parameters
	- Object and Array
	- Destructuring
	- Spread Operator and Rest Parameter
- **TypeScript**
	- Getting Started
	-	- Variable Types
	-	- Arrays
	-	- Type alias & Inteface
	-	- Generic
- **NodeJS**
	-	What is Nodejs
	-	Module
	-	NPM
	-	Synchronous vs Asynchronous
	-	Callback
	-	Promise